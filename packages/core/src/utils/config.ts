import {AppName, ENV, Env} from "./environment";

export interface IBaseConfigData {
  NAME?: AppName;
  API?: string;
  PORT?: number;
}

export type ConfigDataType = IBaseConfigData & any;
export type ConfigSkeleton = { [key in Env]?: ConfigDataType };

export const API_DEV = '/dev';
export const API_TEST = '/test';
export const API_PROD = '/api';

export const conf = (data: ConfigSkeleton, environment = ENV): ConfigDataType => data[environment];


